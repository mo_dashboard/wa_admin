var log4js = require("log4js");


const config = {
	appenders: {
		access: { type: 'file', filename: 'logs/MO_TSEL.log', maxLogSize: 5000, backups: 4 }
	},
	categories: { default: { appenders: ['access'], level: 'debug' } }
};

log4js.configure(config);
const logger = log4js.getLogger('default');

var mysql = require('mysql');
var con = mysql.createConnection({
	host: 'localhost',
	user: 'mo_dashboard',
	password: 'qwePOI#123',
	database: 'mo_dashboard',
	port: 3306
});

var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport({
	service: 'gmail',
	auth: {
		user: 'mo.ebisigma@gmail.com',
		pass: 'qweRTY123$%^'
	}
});

var receiverName;
var receiverEmail;
var receiverNo;

const venom = require('venom-bot');
venom
	.create('dashboard')
	.then((client) => start(client))
	.catch((error) => console.log('blah ' + error));

async function start(client) {
	//GET GROUP

	client.onMessage(async (message) => {
		if (message.body.includes('##') && message.from === '6282261402419-1619067274@g.us') {
			// console.log(message);
			logger.info('START PROCCESS --- New incoming MO message');
			//CHANGE MSG TO OBJ
			let input = message.body.split(new RegExp(/(.*)## (.*) ##/i));
			let arr = input[2].split(' ^ ');
			
			// logger.info(arr);
			
			var apprvl = arr[9];
			var approverEmail = arr[7];

			if (apprvl === '1') {
				logger.info('Inserting to DB \n' + arr);
				// INSERT INTO DB
				arr.push('waiting_approval');
				// console.log('input++: ' +arr);
				var sql =
					'INSERT INTO mo_daily_activity (report_time, working_unit, project_id, activity, request_source, customer_pic_email, internal_pic_email, internal_reviewer_email, action_time, need_approval, approval_status) VALUES (?)';
				var values = [arr];
				con.query(sql, [arr], function (err, result) {
					if (err) throw err;
					logger.info('Number of records inserted: ' + result.affectedRows + '\nActivity ID: ' + result.insertId);
					activityId = parseInt(result.insertId);
				});

				// SELECT USER DATA
				logger.info('Getting Approver Info')
				var sqlSelUser = 'SELECT users.* FROM users, mo_daily_activity WHERE mo_daily_activity.internal_reviewer_email = users.email AND mo_daily_activity.internal_reviewer_email = ?';
				con.query(sqlSelUser, [approverEmail], function (err, resultUser) {
					if (err) throw err;
					// console.log(resultUser[0]);
					receiverName = resultUser[0].full_name;
					receiverEmail = resultUser[0].email;
					receiverNo = resultUser[0].mobile_number;
					logger.info('Email: ' +receiverEmail+ ' - Name: ' +receiverName+ ' - No: ' + receiverNo);

				});

				// SEND EMAIL
				logger.info('Sending Message to '+ receiverEmail)
				var emailBody = 'Hi ' + receiverName + ', <br> Activity ' + arr[3] + ' from project ' + arr[2] + ' need your approval to be proceeded. Please contact EOS of project ' + arr[2] + ' immediately for more information.<br>Thanks.';
				var mailOptions = {
					from: 'mo.ebisigma@gmail.com',
					to: receiverEmail,
					// to: 'achmad.bahri@sigma.co.id',
					subject: 'MO Activity Need Approval',
					html: emailBody
				};

				transporter.sendMail(mailOptions, function (error, info) {
					if (error) {
						logger.error(error);
					} else {
						logger.info('Email sent: ' + info.response);
					}
				});

				// SEND WA CHAT
				logger.info('Sending WA to ' + receiverNo)
				client
					.sendText(receiverNo + '@c.us', 'Hi ' + receiverName + ',\nActivity ' + arr[3] + ' from project ' + arr[2] + ' need your approval to be proceeded.\nPlease reach EOS of project ' + arr[2] + ' immediately for more information.\nThanks.')
					.then((result) => {
						logger.info('Result: ', result); //return object success
					})
					.catch((erro) => {
						logger.error('Error when sending: ', erro); //return object error
					});

					logger.info('New incoming MO message --- END PROCCESS')
			} else {
				// console.log('tidak butuh aprroval')
				logger.info('Inserting to DB \n' + arr);
				var sql =
					'INSERT INTO mo_daily_activity (report_time, working_unit, project_id, activity, request_source, customer_pic_email, internal_pic_email, internal_reviewer_email, action_time, need_approval) VALUES (?)';
				var values = [arr];
				con.query(sql, [arr], function (err, result) {
					if (err) throw err;
					logger.info('Number of records inserted: ' + result.affectedRows + '\nActivity ID: ' + result.insertId);
				});
			}
			con.end();

		} else {
			console.log('cicing');
		}
	});
}